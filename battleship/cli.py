# -*- coding: utf-8 -*-

"""
Console script for battleship.
"""
import click
from battleship import Game


@click.group()
def cli():
    pass


@cli.command()
def start():
    """
    Start game
    """

    print("  ____       _______ _______ _      ______  _____ _    _ _____ _____")
    print(" |  _ \   /\|__   __|__   __| |    |  ____|/ ____| |  | |_   _|  __ \ ")
    print(" | |_) | /  \  | |     | |  | |    | |__  | (___ | |__| | | | | |__) |")
    print(" |  _ < / /\ \ | |     | |  | |    |  __|  \___ \|  __  | | | |  ___/")
    print(" | |_) / ____ \| |     | |  | |____| |____ ____) | |  | |_| |_| |")
    print(" |____/_/    \_\_|     |_|  |______|______|_____/|_|  |_|_____|_|")

    game = Game()
    game.run()


if __name__ == '__main__':
    cli()
