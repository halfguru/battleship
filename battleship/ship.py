# -*- coding: utf-8 -*-

"""
Battleship module.
"""

SHIPS = {
    "carrier": 5,
    "battleship": 4,
    "cruiser": 3,
    "submarine": 3,
    "destroyer": 2
}


class ShipError(Exception):
    """
    Ship object error
    """
    pass


class Ship():
    """
    Ship object representation.

    Args:
        location (list): ship coordinates.
        stype (str): ship type.

    """

    def __init__(self, location, stype):
        if not isinstance(location, list):
            raise ShipError("Ship coordinates should be a list.")
        if not any(isinstance(loc, list) for loc in location):
            raise ShipError("Ship coordinates elements should be a tuple.")
        for loc in location:
            loc.append("ok")
        self._location = location
        self._stype = stype

    @property
    def location(self):
        """
        Ship coordinates

        Returns:
            (list): ship coordinates
        """
        return self._location

    @property
    def stype(self):
        """
        Ship type

        Returns:
            (str): ship type
        """
        return self._stype
    
    def check_status(self):
        """
        Check ship status
        """
        # TODO
        pass
