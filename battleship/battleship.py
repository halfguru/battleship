# -*- coding: utf-8 -*-

"""
Main module.
"""
from random import randint

from ship import Ship
from ship import SHIPS
from board import Board
from board import COLORS
from board import BoardError


class GameError(Exception):
    """
    Game Error
    """
    pass


class Game():
    """
    Game flow
    """

    def __init__(self):
        self.p1_carrier = self.p2_carrier = "carrier"
        self.p1_battleship = self.p2_battleship = "battleship"
        self.p1_cruiser = self.p2_cruiser = "cruiser"
        self.p1_submarine = self.p2_submarine = "submarine"
        self.p1_destroyer = self.p2_destroyer = "destroyer"
        self.p1_ships = [self.p1_carrier, self.p1_battleship,
                         self.p1_cruiser, self.p1_submarine, self.p1_destroyer]
        self.p2_ships = [self.p2_carrier, self.p2_battleship,
                         self.p2_cruiser, self.p2_submarine, self.p2_destroyer]
        self.p1_board = Board()
        self.p2_board = Board()
        self.p1_target_board = Board()
        self.p2_target_board = Board()

    def run(self):
        """
        Run the game.
        """
        colors = [COLORS["green"], COLORS["yellow"],
                  COLORS["blue"], COLORS["magenta"], COLORS["black"]]
        print("\nType your coordinates in the following format --> 0,0")

        # Your turn for game setup
        for index, ship_element in enumerate(SHIPS):
            color = colors[index % len(colors)]
            self.p1_board.print_board()

            # Insert ship coordinates with user input
            print("Where do you want to put your {0} at size {1}".format(
                ship_element, SHIPS[ship_element]) + "?")
            while True:
                head = input("Type the head coordinates: ")
                tail = input("Type the tail coordinates: ")
                try:
                    location = self.process_ship_location(
                        head, tail, SHIPS[ship_element])
                    for loc in location:
                        self.p1_board.set_board_pt(
                            loc[0], loc[1], "x", color=color, setup=True)
                    break
                except GameError as g:
                    print(g)
                except BoardError as b:
                    print(b)
                except ValueError as v:
                    print(v)
                except TypeError as t:
                    print(t)

            # Create ship object
            for game_ship in self.p1_ships:
                if game_ship == ship_element:
                    game_ship = Ship(location, ship_element)

        # Opponent turn game setup
        print("\nOpponent turn game setup")
        for index, ship_element in enumerate(SHIPS):
            color = colors[index % len(colors)]
            while True:
                head = str(randint(0, 9)) + "," + str(randint(0, 9))
                tail = str(randint(0, 9)) + "," + str(randint(0, 9))
                try:
                    location = self.process_ship_location(
                        head, tail, SHIPS[ship_element])
                    for loc in location:
                        self.p2_board.set_board_pt(
                            loc[0], loc[1], "x", color=color, setup=True)
                    break
                except Exception as e:
                    pass

            # Create ship object
            for game_ship in self.p2_ships:
                if game_ship == ship_element:
                    game_ship = Ship(location, ship_element)
        self.p2_board.print_board()

        # Game loop
        while True:
            # Your turn
            print("\nAttack Phase\n")
            print("Your board")
            self.p2_board.print_board()
            print("Target board")
            self.p1_target_board.print_board()

            for ship in self.p1_ships:
                # TODO
                pass

            guess = input("Choose a location to attack: ")
            guess_x = int(guess.split(",")[1])
            guess_y = int(guess.split(",")[0])
            if "x" in self.p2_board.get_board_pt(guess_x, guess_y).lower():
                print("\nIt's a hit!")
                self.p2_board.set_board_pt(
                    guess_x, guess_y, color=COLORS["red"], value="o")
                self.p1_target_board.set_board_pt(
                    guess_x, guess_y, color=COLORS["red"], value="o")
            else:
                print("\nIt's a miss!")
                self.p1_target_board.set_board_pt(
                    guess_x, guess_y, color=COLORS["white"], value="x")

            # Opponent turn
            print("\nDefense Phase\n")
            # self.p1_board.print_board()
            guess = str(randint(0, 9)) + "," + str(randint(0, 9))
            guess_x = int(guess.split(",")[1])
            guess_y = int(guess.split(",")[0])
            if "x" in self.p1_board.get_board_pt(guess_x, guess_y).lower():
                print("\nIt's a hit!")
                self.p1_board.set_board_pt(
                    guess_x, guess_y, color=COLORS["red"], value="o")
            else:
                print("\nIt's a miss!")

    def process_ship_location(self, head, tail, ship_len):
        """
        Process ship head and tail coordinates and transform it in 2D array

        Args:
            head(str): Ship head
            tail(str): Ship tail
            ship_len(int): Ship length
            stype(str): Ship type

        Returns:
            (list): Ship location
        """
        location = [None] * ship_len
        head_x = int(head.split(",")[1])
        head_y = int(head.split(",")[0])
        tail_x = int(tail.split(",")[1])
        tail_y = int(tail.split(",")[0])
        start_x = head_x
        end_x = tail_x
        start_y = head_y
        end_y = tail_y
        if head_x > tail_x:
            start_x, end_x = tail_x, head_x

        if head_y > tail_y:
            start_y, end_y = tail_y, head_y

        for i in range(ship_len):
            if start_x == end_x:
                if abs(start_y - end_y) != ship_len - 1:
                    raise GameError(
                        "Coordinates given are not of size {0}".format(ship_len))
                location[i] = [start_x, start_y + i]
            elif start_y == end_y:
                if abs(start_x - end_x) != ship_len - 1:
                    raise GameError(
                        "Coordinates given are not of size {0}".format(ship_len))
                location[i] = [start_x + i, start_y]
        return location
