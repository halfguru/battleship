# -*- coding: utf-8 -*-

"""
Board module.
"""
from colorama import Fore, Style

COLORS = {"green": Fore.GREEN,
          "yellow": Fore.YELLOW,
          "blue": Fore.BLUE,
          "magenta": Fore.MAGENTA,
          "black": Fore.BLACK,
          "white": Fore.WHITE,
          "red": Fore.RED,
          "cyan": Fore.CYAN}


class BoardError(Exception):
    """
    Board Error
    """
    pass


class Board():
    """
    Board object representation
    """

    def __init__(self):
        self._board = [["-" for i in range(10)] for j in range(10)]

    @property
    def board(self):
        """
        Board state

        Returns:
            (list): Board state
        """
        return self._board

    def set_board_pt(self, x, y, value, color, setup=False):
        """
        Set board value at coordinate

        Args:
            x (int): board point x coordinate
            y (int): board point y coordinate
            value (str): board point value
            color (str): board value color
            setup (bool): if game setup phase
        """
        if "x" in self.board[x][y] and setup:
            raise BoardError(
                "Can't set value at {0},{1}, it's already occupied.".format(x, y))
        if "o" in self.board[x][y]:
            raise BoardError(
                "Can't set value at {0},{1}, it's already occupied.".format(x, y))
        self.board[x][y] = color + value + Fore.WHITE

    def get_board_pt(self, x, y):
        """
        Get board value at coordinate

        Returns:
            (str): Board coordinate value
        """
        return self.board[x][y]

    def print_board(self):
        """
        Print current board
        """
        for i in range(len(self.board)):
            print(Fore.CYAN + str(i) + " ", end="")
        print(Style.RESET_ALL, end="")
        print("")
        for index, row in enumerate(self.board):
            print(" ".join(row) +
                  " {0}".format(Fore.CYAN + str(index) + Fore.WHITE))
