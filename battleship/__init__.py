# -*- coding: utf-8 -*-

"""Top-level package for battleship."""

__author__ = """Simon Ho"""
__email__ = 'simon.ho@mail.mcgill.ca'
__version__ = '0.1.0'
