==========
battleship
==========


.. image:: https://img.shields.io/pypi/v/battleship.svg
        :target: https://pypi.python.org/pypi/battleship

.. image:: https://img.shields.io/travis/halfguru/battleship.svg
        :target: https://travis-ci.org/halfguru/battleship

.. image:: https://readthedocs.org/projects/battleship/badge/?version=latest
        :target: https://battleship.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://battleship.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
